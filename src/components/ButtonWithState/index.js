import React from "react";

import ButtonWithState from "./ButtonWithState";

const Component = props => <ButtonWithState {...props} />;

export default Component;
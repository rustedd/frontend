import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import * as Styles from "./McqBuilderStyled"

const McqBuilderPresentational = props => {
  return (
    <Styles.McqWrapper>
      <Form>
        <FormGroup row>
          <Label for="title" sm={2}>Question</Label>
          <Col sm={10}>
            <Input 
              type="text" 
              name="title" 
              id="title" 
              value={props.question}
              onChange={props.onChangeNotes}
              placeholder="enter title" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="text" sm={2}>Content</Label>
          <Col sm={10}>
            <Input 
              type="textarea" 
              name="text" 
              id="text" 
              value={props.text}
              onChange={props.onChangeNotes}
              placeholder="enter content" />
          </Col>
        </FormGroup>
      </Form>
      <div>
      <Button onClick={props.saveNotes}>save</Button>
      <Button onClick={props.deleteNote}>delete</Button>
        <div>{props.message}</div>
      </div>
    </Styles.McqWrapper>
  )
}

export default McqBuilderPresentational;
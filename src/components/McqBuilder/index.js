import React from "react";

import McqBuilderContainer from "./Containers/McqBuilderContainer"

const Component = props => <McqBuilderContainer {...props} />

export default Component
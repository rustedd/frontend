import React from "react";

import McqBuilderPresentational from "../Presentationals/McqBuilderPresentational"

class McqBuilderContainer extends React.Component {
  constructor(props) {
    super();
    this.state = {

    }
  }

  render() {    
    return (
      <McqBuilderPresentational 
        particles={this.props.particle}
      />
    )
  }
}

export default McqBuilderContainer;
import styled from "styled-components"

const Button = styled.div`
  background-color: #292929;
  color: white;
  box-sizing: border-box;
  padding: 15px 25px;
  align-self: center;
  display: flex;
  text-align: center;
  justify-content: center;
  border-radius: 10px;
  margin-top: 25px;
  cursor: pointer;
`

export default Button
import styled from "styled-components"

export const HeadingInput = styled.input`
  border: 0;
  outline: 0;
  font-size: 32px;
  font-family: 'Work Sans', sans-serif;
  font-weight: 700;
  background-color: transparent;
`;

export const SubHeadingInput = styled.input`
  border: 0;
  outline: 0;
  font-size: 24px;
  width: 100%;
  font-family: 'Work Sans', sans-serif;
  font-weight: 700;
  background-color: transparent;
`;

export const OrdinaryInput = styled.input`
  border: 0;
  outline: 0;
  font-size: 18px;
  width: 100%;
  background-color: transparent;
`;

export const H2Input = styled.input`
  border: 0;
  outline: 0;
  font-size: 22px;
  width: 100%;
  background-color: transparent;
  font-family: 'Lato', sans-serif;
`

export const MultilineInput = styled.textarea`
  border: 0;
  outline: 0;
  font-size: 18px;
  width: 100%;
  background-color: transparent;
  font-family: 'Lato', sans-serif;
`

export const TitleInput = styled.input`
  border: 0;
  outline: 0;
  font-size: 18px;
  width: 100%;
  font-size: 24px;
  background-color: transparent;
`

export const ParticleHeading =styled.h3`
  color: #34323f;
  font-family: 'Work Sans', sans-serif;
  font-weight: 700;

`;

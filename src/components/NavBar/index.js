import React from 'react'

import NavBarContainer from "./Containers/NavBarContainer"

const Component = props => <NavBarContainer {...props} />

export default Component;
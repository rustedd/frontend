import React from "react";
import {
  Navbar,
  NavbarBrand,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap"
import Cookies from "js-cookie"


const NavBarPresentational = props => {
  return (
    <Navbar color="light" light expand="md">
    <NavbarBrand href="/">dashboard</NavbarBrand>
      <Nav className="mr-auto" navbar>

      </Nav>
      <UncontrolledDropdown nav inNavbar style={{listStyle: 'none'}}>
          <DropdownToggle nav caret>
            {Cookies.get('email')}
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem  onClick={props.logout}>
              Logout
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
  </Navbar>
  )
}

export default NavBarPresentational;
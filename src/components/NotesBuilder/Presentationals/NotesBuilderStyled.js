import styled from "styled-components";

export const NotesWrapper = styled.div`
  padding: 20px;
  /* margin-bottom: 20px; */
  border-radius: 10px;
  min-width:450px;
  background-color: white;
  box-shadow: 0 2px 15px 0 rgba(0,0,0,0.1), 0 0px 0px 0 rgba(0,0,0,0.19);
  &:hover {
  }
`;

export const BottomButtonWrapper = styled.div`
  margin-top: 16px;
  display: flex;
  flex-direction:row;
  /* justify-content:flex-end; */
`;

export const ParticleId = styled.div`
  
`
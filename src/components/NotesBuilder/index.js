import React from 'react';

import NotesBuilderContainer from "./Containers/NotesBuilderContainer"

const Component = props => <NotesBuilderContainer {...props} />

export default Component;
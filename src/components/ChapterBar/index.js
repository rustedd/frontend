import React from "react";

import ChapterBarContainer from "./Containers/ChapterBarContainer"

const Component = props => <ChapterBarContainer {...props}/>

export default Component;
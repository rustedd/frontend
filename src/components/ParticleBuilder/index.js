import React from "react";

import ParticleBuilder from "./Containers/ParticleBuilderContainer"

const Component = props => <ParticleBuilder {...props} />

export default Component;
import React from "react";
import { 
  Dropdown, 
  DropdownToggle, 
  DropdownMenu, 
  DropdownItem,
  Form,
  Input,
  FormGroup,
  InputGroupAddon,
  InputGroup,
  Button as ReactStrapButton
} from 'reactstrap';
import { InfoCircleOutlined, DeleteOutlined } from "@ant-design/icons"
import { Tooltip, Button, Popover, Popconfirm } from 'antd'
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import HowToAddFromGSheets from "./HowToAddFromGSheets"

import mcq from "./mcq.jpg"
import notes from "./notes.jpg"

import NotesBuilder from "../../NotesBuilder"
import * as Styles from "./ParticleBuilderStyled"
import * as Formed from "../../../components/StyledItems/Form"

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 1,
  // margin: `0 0 ${grid}px 0`,

  // change background colour if dragging
  background: isDragging ? "lightgreen" : "transparent",
  borderRadius: '10px',
  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "lightblue" : "transparent",
  padding: grid,
  borderRadius: '10px'
});


class ParticleBuilderPresentational extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }

  onDragEnd = (result) => {
    this.props.reorderParticles(result.source.index, result.destination.index)
  }

  render() {
    let { props } = this;
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Styles.ParticleWrapper>
        <Form style={{paddingLeft: '10px', maxWidth: '600px'}}>
          <FormGroup>
            <Formed.HeadingInput 
              type="text" 
              name="title" 
              value={props.title}
              onChange={props.onChangeChapterDetails}
              placeholder="enter chapter title" 
            />
          </FormGroup>
          <FormGroup>
            <Formed.SubHeadingInput
              type="text" 
              name="description" 
              id="description" 
              value={props.description}
              onChange={props.onChangeChapterDetails}
              placeholder="enter chapter description" />

          </FormGroup>
          <div style={{display:'flex', justifyContent:'space-between'}}>
            {/* <ButtonWithState 
              state={props.patchButtonColor} 
              text="Update Headings"
              onClick={props.patchChapterDetails}/> */}
            <div style={{marginLeft: '10px'}}>
              <Tooltip title={props.isPublished ? "Click to unpublish this lesson on the app" : "Click to publish this lesson on the app"}>
                <Button type="primary" onClick={props.toggleChapterPublish}>{props.isPublished ? "Unpublish" : "Publish"}</Button>
                {/* <ButtonWithState
                  state={props.patchButtonColor} 
                  text={props.isPublished ? "Unpublish" : "Publish"}
                  onClick={props.toggleChapterPublish}/> */}
              </Tooltip>
            </div>
            <div>
              <Popconfirm
                title={`Are you sure you want to delete this chapter permanently?`}
                onConfirm={() => props.deleteChapter(props.selectedChapterId)}
                onCancel={() => null}
                okText="Yes"
                cancelText="No"
                placement="topRight"
              >
                <DeleteOutlined />  
              </Popconfirm>
            </div>
          </div>
          <br />
          <div style={{display: 'flex', alignItems: 'center'}}>
            <InputGroup>
              <Input 
                name="google-sheet-url" 
                id="googleSheetUrl"
                onChange={props.changeGoogleSheetUrl}
                placeholder="put google sheet url here to import from google sheet" />
              <InputGroupAddon addonType="append">
                <Popover title="How to">
                  <ReactStrapButton 
                    onClick={props.importFromGoogleSheet}>
                    Import
                  </ReactStrapButton>
                </Popover>
                </InputGroupAddon>
            </InputGroup>
            <Popover title="How to add from google sheets" content={HowToAddFromGSheets}>
              <InfoCircleOutlined style={{ marginLeft: '10px' }}/>
            </Popover>
          </div>
        </Form>
        <div style={{height: "45px", marginTop: '45px'}}></div>
        <div style={{maxWidth: '620px'}}>
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}
              >
                {props.particles.map((particle, index) => (
                  <Draggable key={particle._id} draggableId={particle._id} index={index}>
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style
                        )}
                      >
                        <NotesBuilder 
                          key={particle._id}
                          particle={particle}
                          selectedChapterId={props.selectedChapterId}
                          getLessonData={props.getLessonData}
                          particles={props.particles}
                          deleteParticle={props.deleteParticle}
                        /> 
                        {/* <div>{particle.rank}</div> */}
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </div>
        <div>
          <Dropdown isOpen={props.dropdownOpen} toggle={props.toggleDropdown}>
            <DropdownToggle caret>
              Add new
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem header>Select type to add</DropdownItem>
              <Popover placement="right" title={"Multiple Choice Question"} content={mcqContent}>
                <DropdownItem onClick={() => {props.addNewParticle("MCQ")}}>Multiple Choice Question</DropdownItem>
              </Popover>
              <Popover placement="right" title={"Note"} content={notesContent}>
                <DropdownItem onClick={() => {props.addNewParticle("NOTE")}}>Notes</DropdownItem>
              </Popover>
              <DropdownItem onClick={() => {props.addNewParticle("MEANING")}}>Meaning</DropdownItem>
              <DropdownItem onClick={() => {props.addNewParticle("CONVERSATION")}}>Conversation</DropdownItem>
              <DropdownItem onClick={() => {props.addNewParticle("JUMBLEDWORDS")}}>Jumbled Words</DropdownItem>
              <DropdownItem onClick={() => {props.addNewParticle("STACKEDNOTES")}}>Stack of Notes</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
      </Styles.ParticleWrapper>
      </DragDropContext>
    ) 
  }
}

const mcqContent = (
  <div style={{maxWidth: '220px'}}>
    <img src={mcq} alt="mcq" />
    <p>Questions which can be used to learn. Has tips and hints to assist in the learning process.</p>
  </div>
);

const notesContent = (
  <div style={{maxWidth: '220px'}}>
    <img src={notes} alt="notes" />
    <p>Notes are to give details about a particular topic and explain it.</p>
  </div>
);

export default ParticleBuilderPresentational;
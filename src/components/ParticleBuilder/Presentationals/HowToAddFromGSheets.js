import React from 'react';

const HowToAddFromGSheets = props => {
  return (
    <div style={{maxWidth: '300px'}}>
      <div>
        Create a google sheet that looks like <a href="https://docs.google.com/spreadsheets/d/1nnk9dLwGhDc7FBS584952i9RBwBNm3hGpnm9z-NSp0E/edit#gid=0">this</a>. <br/>
        Ensure that the 1st row remains intact.  <br/>
        The types can be either be MCQ or NOTE depending on your need. <br/>
        The correct option also need to be added <br/>
        After adding the content make the google sheet public <br/>
        This can be done by clicking on the Share button on the right side top and making the document accessible to anyone with
        a link
      </div>
    </div>
  )
}

export default HowToAddFromGSheets
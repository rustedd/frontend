import React from 'react';
import axios from "axios";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


import "./App.css";
import 'antd/dist/antd.css';
import "./index.css";
import 'bootstrap/dist/css/bootstrap.min.css';

import Home from './screens/Home';
import Auth from './screens/Auth';
import ContentEditorScreen from './screens/ContentEditor';
import ProtectedRoute from "./components/ProtectedRoute"

if(process.env.NODE_ENV === "production"){
  axios.defaults.baseURL = "https://api.desksandbenches.com/"
} else {
  axios.defaults.baseURL = "http://localhost:3001"
}

function App() {

  return (
    <Router>
      <Switch>
        <Route path="/auth">
          <Auth />
        </Route>
        <ProtectedRoute path="/app/:appId" component={ContentEditorScreen}/>
        <ProtectedRoute path="/" component={Home} exact/>
      </Switch>
  </Router>
  )
}

export default App;

import styled from "styled-components";

export const Wrapper = styled.div`
  /* width: 240px; */
  /* background-color: pink; */
  /* height: 100vh; */
  display: flex;
  width: 100%;
`

export const CrazyWrapper = styled.div`
  height: 100vh;
  display: flex;
  width: 100%;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: auto;
`
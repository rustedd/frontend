import React from 'react';

import ContentEditorContainer from './Containers/ContentEditorContainer';

const Component = (props) => <ContentEditorContainer {...props} />;

export default Component;

import React from "react";

import HomeContainer from "./Containers/HomeContainer";

const Component = props => <HomeContainer {...props} />;

export default Component;
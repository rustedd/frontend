import React from "react";
import axios from "axios";
import { Redirect } from "react-router-dom"

import Cookies from "js-cookie"

import AuthPresentational from "../Presentationals/AuthPresentational"


class AuthContainer extends React.Component {
  constructor(props) {
    super();
    this.state = {
      screen: "LOGIN",
      email: "",
      password: "",
      passwordConfirm: "",
      message: "",
      loggedIn: false,
      networkCallState: "idle"
    }
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      message: "",
      networkCallState: "idle"
    })
  }

  toggleScreen = () => {
    if(this.state.screen === "LOGIN"){
      this.setState({
        screen: "SIGNUP",
      })
    } else {
      this.setState({
        screen: "LOGIN"
      })
    }
  }

  submitForm = () => {
    if(!this.state.email || !this.state.password){
      this.setState({
        message: "fill all fields"
      });
      return;
    }
    let url = '/user/login';
    let data = {
      email: this.state.email,
      password: this.state.password
    };
    if(this.state.screen === "SIGNUP"){
      url = '/user/signup';
      data.passwordConfirm = this.state.passwordConfirm
    }
    
    this.setState({
      networkCallState: "pending"
    })

    axios
      .post(url, data)
      .then(resp => {
        Cookies.set('token', resp.data.token);
        Cookies.set('email', resp.data.email);
        this.setState({
          loggedIn: true,
          networkCallState: "resolved"
        })
      })
      .catch(err => {
        this.setState({
          message:err.response?.data?.message? err.response?.data?.message: "Something went wrong" ,
          networkCallState: "rejected"
        })
      })
  }

  render() {
    if(this.state.loggedIn){
      return <Redirect to='/' />
    }
    return (
      <AuthPresentational 
        onChange={this.onChange}
        email={this.state.email}
        networkCallState={this.state.networkCallState}
        toggleScreen={this.toggleScreen}
        password={this.state.password}
        passwordConfirm={this.state.passwordConfirm}
        screen={this.state.screen}
        submitForm={this.submitForm}
        message={this.state.message}
      />
    )
  }
}

export default AuthContainer;
import React from "react";

import * as Styles from "./AuthStyled";
import Button from "../../../components/ButtonWithState"

const AuthPresentational = (props) => {
  return (
    <Styles.Wrapper>
      <Styles.FormWrapper></Styles.FormWrapper>
      <Styles.FormWrapper>
        {props.screen === "SIGNUP" ? (
          <Styles.Heading>Signup</Styles.Heading>
        ) : (
          <Styles.Heading>Login</Styles.Heading>
        )}
        <Styles.InputField 
          name="email"
          onChange={props.onChange}
          placeholder="Enter your email"></Styles.InputField>
        <Styles.InputField 
          name="password"
          onChange={props.onChange}
          placeholder="Enter password" type="password"></Styles.InputField>
        {props.screen === "SIGNUP" ? (
          <Styles.InputField 
            name="passwordConfirm"
            onChange={props.onChange}
            placeholder="Confirm password" type="password"></Styles.InputField>
        ) : null}

        <Styles.ButtonWrapper>
          <Button 
            state={props.networkCallState}
            onClick={props.submitForm}
            text={props.screen === "SIGNUP" ? "Signup" : "Login"} />
        </Styles.ButtonWrapper>

        {props.screen === "SIGNUP" ? (
          <Styles.LoginText>
            {"I already have an account "}
            <Styles.LoginTextClickable
              onClick={() => {
                props.toggleScreen();
              }}
            >
              Login
            </Styles.LoginTextClickable>
            {" instead"}
          </Styles.LoginText>
        ) : (
          <Styles.LoginText>
            {" "}
            <Styles.LoginTextClickable
              onClick={() => {
                props.toggleScreen();
              }}
            >
              Signup
            </Styles.LoginTextClickable>{" "}
            instead
          </Styles.LoginText>
        )}
        <Styles.LoginText style={{color:"red"}}>
          {props.message}
        </Styles.LoginText>
      </Styles.FormWrapper>
    </Styles.Wrapper>
  );
};

export default AuthPresentational;

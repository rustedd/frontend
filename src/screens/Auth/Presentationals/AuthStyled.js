import styled from "styled-components";

export const Heading = styled.h1`
  font-family: 'Bitter', serif;
  font-weight: 400;
  margin: 32px 0px;
`

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  font-family: 'Bitter', serif;
`

export const InputField = styled.input`
  font-family: 'Bitter', serif;
  font-weight: 800;
  border: none;
  border-bottom: 2px solid #a2a2a2;
  width: 100%;
  max-width: 320px;
  display: flex;
  justify-content: center;
  align-items: center;
  /* text-align: center; */
  margin: 30px 0px;
`

export const ButtonWrapper = styled.div`
  max-width: 320px;
`

export const FormWrapper = styled.div`
  width: 50%;
  padding: 10%;

`

export const LoginText = styled.div`
  font-family: 'Bitter', serif;
  font-weight: 400;
  margin-top: 15px;
`

export const LoginTextClickable = styled.span`
  text-decoration: underline;
  color: grey;
  cursor: pointer;
`